# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.6](https://gitlab.com/curedas-c/school-project-parent/compare/v0.0.5...v0.0.6) (2021-10-22)


### Features

* init capacitor ([180a12d](https://gitlab.com/curedas-c/school-project-parent/commit/180a12d3cbe27b4361d265bebd103fab3c6372b6))

### [0.0.5](https://gitlab.com/curedas-c/school-project-parent/compare/v0.0.4...v0.0.5) (2021-10-21)

### Features

- adds ngxs ([76c94d3](https://gitlab.com/curedas-c/school-project-parent/commit/76c94d393450d4201d0ffd3c3df040460be56607))

### [0.0.4](https://gitlab.com/curedas-c/school-project-parent/compare/v0.0.3...v0.0.4) (2021-10-21)

### Features

- use cypress for e2e tests ([f5aff13](https://gitlab.com/curedas-c/school-project-parent/commit/f5aff138b94fda4dbc91a74c86516161743f269e))

### [0.0.3](https://gitlab.com/curedas-c/school-project-parent/compare/v0.0.2...v0.0.3) (2021-10-21)

### Features

- adds nebular ([397e676](https://gitlab.com/curedas-c/school-project-parent/commit/397e67622d89850979f5a68260eb3465514c88a8))

### 0.0.2 (2021-10-21)

### Features

- adds and configure husky ([9564719](https://gitlab.com/curedas-c/school-project-parent/commit/95647194cdbcc3dcfd92548130c92d85b59a0d16))
- adds commitizen ([8adb931](https://gitlab.com/curedas-c/school-project-parent/commit/8adb931e780abad90ce32a9cd468dfe108c1a648))
- adds standard-version ([d0c50e7](https://gitlab.com/curedas-c/school-project-parent/commit/d0c50e783968532cbc510b7fcf5eebdc6c64ae26))
- configure lint-staged ([d526df7](https://gitlab.com/curedas-c/school-project-parent/commit/d526df788d7fd77c3cdef793c47b0857aa0aa92c))
- updates lint-staged and eslint config ([a635bf8](https://gitlab.com/curedas-c/school-project-parent/commit/a635bf895a948713e9a32ff9df16fb64bded5e57))
